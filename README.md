# Landmarks

Code for the `landmark recognition` and for the `landmark retreival` competition hosted on kaggle.com in March and April 2018.

Links:

https://www.kaggle.com/c/landmark-recognition-challenge
https://www.kaggle.com/c/landmark-retrieval-challenge