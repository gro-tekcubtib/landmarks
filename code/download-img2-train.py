import multiprocessing
import os
from io import BytesIO
from urllib import request
import pandas as pd
import re
import tqdm
from PIL import Image

# set files and dir
DATA_FRAME, OUT_DIR = pd.read_csv('../inputs/train.csv'), '../inputs/train'  # recognition challenge
# DATA_FRAME, OUT_DIR = pd.read_csv('../inputs/index.csv'), '../inputs/index'  # retrieval challenge
#DATA_FRAME, OUT_DIR = pd.read_csv('../inputs/test.csv'), '../inputs/test/test'  # test data

# preferences
DOWNLOAD_SIZE = 350 # image resolution to be downloaded (max(width, height))
TARGET_SIZE = 256  # image resolution to be stored
IMG_QUALITY = 95  # JPG quality
NUM_WORKERS = 20  # Num of CPUs

def overwrite_urls(df):
    def reso_overwrite(url_tail, reso=DOWNLOAD_SIZE):
        pattern = 's[0-9]+'
        search_result = re.match(pattern, url_tail)
        if search_result is None:
            return url_tail
        else:
            return 's{}'.format(reso)

    def join_url(parsed_url, s_reso):
        parsed_url[-2] = s_reso
        return '/'.join(parsed_url)

    parsed_url = df.url.apply(lambda x: x.split('/'))
    train_url_tail = parsed_url.apply(lambda x: x[-2])
    resos = train_url_tail.apply(lambda x: reso_overwrite(x, reso=DOWNLOAD_SIZE))

    overwritten_df = pd.concat([parsed_url, resos], axis=1)
    overwritten_df.columns = ['url', 's_reso']
    df['url'] = overwritten_df.apply(lambda x: join_url(x['url'], x['s_reso']), axis=1)
    return df


def parse_data(df):
    key_url_list = [line for line in df.values] # Lst of numpy array (id, url, landmark_id)
    return key_url_list


def download_image(key_url):
    (key, url, category) = key_url
    filename = os.path.join(OUT_DIR, str(category), '{}.jpg'.format(key))

    os.makedirs(os.path.join(OUT_DIR, str(category)), exist_ok=True) # Create dir if not exists

    if os.path.exists(filename):
        print('Image {} already exists. Skipping download.'.format(filename))
        return 0

    try:
        response = request.urlopen(url)
        image_data = response.read()
    except:
        print('Warning: Could not download image {} from {}'.format(key, url))
        return 1

    try:
        pil_image = Image.open(BytesIO(image_data))
    except:
        print('Warning: Failed to parse image {}'.format(key))
        return 1

    try:
        pil_image_rgb = pil_image.convert('RGB')
    except:
        print('Warning: Failed to convert image {} to RGB'.format(key))
        return 1

    try:
        if pil_image_rgb.size[0] < pil_image_rgb.size[1]:
            wpercent = (TARGET_SIZE/float(pil_image_rgb.size[0]))
            hsize = int((float(pil_image_rgb.size[1])*float(wpercent)))
            pil_image_resize = pil_image_rgb.resize((TARGET_SIZE,hsize))
        else:
            wpercent = (TARGET_SIZE/float(pil_image_rgb.size[1]))
            hsize = int((float(pil_image_rgb.size[0])*float(wpercent)))
            pil_image_resize = pil_image_rgb.resize((hsize,TARGET_SIZE))
    except:
        print('Warning: Failed to resize image {}'.format(key))
        return 1

    try:
        pil_image_resize.save(filename, format='JPEG', quality=IMG_QUALITY)
    except:
        print('Warning: Failed to save image {}'.format(filename))
        return 1

    return 0

def loader(df):
    os.makedirs(OUT_DIR, exist_ok=True)

    key_url_list = parse_data(df)
    pool = multiprocessing.Pool(processes=NUM_WORKERS)
    failures = sum(tqdm.tqdm(pool.imap_unordered(download_image, key_url_list),
                             total=len(key_url_list)))
    print('Total number of download failures:', failures)
    pool.close()
    pool.terminate()

# now, start downloading
if __name__ == '__main__':
    loader(overwrite_urls(DATA_FRAME))
