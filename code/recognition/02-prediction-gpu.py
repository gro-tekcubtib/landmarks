"""
Alex-Antoine Fortin
aturday March 31st 2018
Description
Makes the predictions.
"""
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)

import os, io, imp, itertools, numpy as np, pandas as pd
from keras import backend as K
from keras.models import load_model
from tqdm import tqdm
t = imp.load_source('tools', '../code/recognition/tools.py')
MODEL_NAME = 'ResNet50'
epoch=18
LOAD_MODEL_FROM_FILE = 'Epoch{}-Loss0.067102-Acc0.985193.h5'.format(epoch) #'Epoch12-Loss0.252116-Acc0.949178.h5' # # Str. Filename
TARGET_SIZE = (200, 200) # Tuple. Dimensions of crop size to use as input
NUM_CROP_BY_IMG = 5 # Number of rnd crop to take for each landmark.
#                     return avg pred for all crop
PATH_TO_DATA = '../masked_inputs/test/test'
num_test_products = 115838

#=====================
# Getting list of imgs
#=====================
lst_img = os.listdir(PATH_TO_DATA)

#=========================
# Get mapping from idx2cat
#=========================
mapping = pd.read_csv('../mapping-cat-idx.csv', dtype=str)
idx2cat = {k:v for k,v in mapping[['idx', 'cat']].values}
assert idx2cat['4'] == '1000'

#================================
#Defining model & loading weights
#================================
with tf.device('/gpu:0'):
    model = load_model('../models/{}/{}'.format(MODEL_NAME, LOAD_MODEL_FROM_FILE))
    print('Loaded model from file: {}'.format(LOAD_MODEL_FROM_FILE))

#=======
#Predict
#=======
submission_df = pd.DataFrame(columns=['id', 'landmarks', 'score'])
from matplotlib.pyplot import imread
from datetime import datetime
with tqdm(total=num_test_products) as pbar: #XXX countsreplace 5 by num_test_products
    for idx, fname in enumerate(lst_img):
        landmark_id = fname.replace('.jpg', '')
        img_path = os.path.join(PATH_TO_DATA, fname)
        #batch_x = np.empty((NUM_CROP_BY_IMG, TARGET_SIZE[0], TARGET_SIZE[1], 3),
        #                    dtype=K.floatx())
        img = imread(fname=img_path).astype(np.float32) # Numpy array of the image (MxNx3)
        #img = t.preprocess_input(img) # t.random_crop always return a normalized img
        tot_crops, tot_count = t.random_crop(img, TARGET_SIZE, train_or_test='test') # t.random_crop returns preprocessed crops as-of 20180331
        tot_crops = np.expand_dims(tot_crops, axis=0)
        for j in range(NUM_CROP_BY_IMG-1): #3-crop
            crops, counts = t.random_crop(img, TARGET_SIZE, train_or_test='test')
            crops = np.expand_dims(crops, axis=0)
            tot_crops = np.append(tot_crops, crops, axis=0)
            tot_count+=counts
            # Add the image to the batch.
            #batch_x[i*3:(i+1)*3,:] = my_crops
        if tot_count >= 9*NUM_CROP_BY_IMG+1:
            prediction_cat_idx, prediction_cat, prediction_score = None, None, None
        else:
            pred = np.mean(model.predict(tot_crops, batch_size=tot_crops.shape[0]), axis=0)
            prediction_cat_idx = pred.argmax() #XXX: need landmark number not idx
            prediction_cat = idx2cat[str(prediction_cat_idx)]
            prediction_score = pred[pred.argmax()]
        tmp_dict = {'id': landmark_id, 'landmarks': prediction_cat, 'score': prediction_score}
        #print(tmp_dict)
        submission_df = submission_df.append(tmp_dict, ignore_index=True)
        pbar.update()


submission_df['landmarks'] = submission_df[['landmarks', 'score']].apply(lambda x: ' '.join([str(x[0]),str(x[1])]) if type(x[0])!=type(None) else None, axis=1)
submission_df.to_csv('../submission-ResNet50-tmp18.csv', index=False)

#===========================================================================
# Adding missing landmarks id not downloaded because their picture were gone
#===========================================================================
sample_submission = pd.read_csv('../inputs/sample_submission.csv', header=0)

def isItIn(term, mylist):
  return '' if term in mylist else term

from joblib import Parallel, delayed
add_me = Parallel(n_jobs=20)(delayed(isItIn)(term, mylist=submission_df.id.unique().tolist()) for term in sample_submission.id.unique().tolist())
add_me = [ x for x in add_me if x!= '']
for things in add_me:
  submission_df = submission_df.append({'id': things, 'landmarks': None, 'score': None}, ignore_index=True)

#================
# Writing to disk
#================
submission_df[['id', 'landmarks']].to_csv('../submission-{}-{}-epoch{}.csv'.format(MODEL_NAME, datetime.today().strftime('%Y%m%d-clocktime%H%M%S'), epoch), index=False)
