import pandas as pd

pred20 = pd.read_csv('./submission-ResNet50-tmp20.csv', header=0)
predold = pd.read_csv('./submission-ResNet50-smaller_train-20180501-clocktime190401.csv', header=0)

pred20[['yhat', 'score']] = pred20.landmarks.str.split(' ', expand=True)
predold[['yhat', 'score']] = predold.landmarks.str.split(' ', expand=True)
pred20['score'] = pred20.score.astype(float)
predold['score'] = predold.score.astype(float)

df = predold.merge(right=pred20, on='id', how='left')

df['yhat'] = df.yhat_x
df['score'] = df.score_x

df.loc[df.score_x<df.score_y, 'yhat'] = df.loc[df.score_x<df.score_y, 'yhat_y'] 
df.loc[df.score_x<df.score_y, 'score'] = df.loc[df.score_x<df.score_y, 'score_y']
df.sample(10)

df_export = df[['id', 'yhat', 'score']]
df_export['landmarks'] = df_export.yhat + ' ' + df_export.score.astype(str)
df_export.sample(4)
df_export.shape
df_export.tail()
df_export[['id', 'landmarks']].to_csv('submission-ResNet50-combine-epoch15-epoch20.csv', index=False)
