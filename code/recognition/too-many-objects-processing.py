import pandas as pd
import numpy as np

df = pd.read_csv('../subs/last-submission/submission-ResNet50-combine-epoch15-epoch20.csv', header=0)
img_info = pd.read_csv('../mask-rcnn-object_detection_summary.csv', header=0)
img_info['id'] = img_info.img.str[:-4]

thresh = 0.70
thresh_bad_obj_count = 7 # inclusive


high_unimportant_area = img_info.loc[img_info.percent_area_masks>thresh, :].reset_index(drop=True)
high_unimportant_area['high_masks'] = 1

df = df.merge(right=high_unimportant_area[['id', 'high_masks']], how='left')
df['high_masks'] =  df['high_masks'].fillna(0)

df.loc[df.high_masks==1, 'landmarks'] = np.nan # remove predictions when lots of non-landmarks objects

#np.percentile(img_info.percent_area_masks, range(0,105,5))


# Penalizing images with object that would not be found near a landmark:

myDict = {'BG': 1, 'person': 0, 'bicycle': 0, 'car': 0, 'motorcycle': 0, 'airplane': 0,
                 'bus': 0, 'train': 0, 'truck': 0, 'boat': 0, 'traffic light': 0,
                 'fire hydrant': 0, 'stop sign': 0, 'parking meter': 0, 'bench': 0, 'bird': 0,
                 'cat': 0, 'dog': 0, 'horse': 0, 'sheep': 0, 'cow': 0, 'elephant': 0, 'bear': 0,
                 'zebra': 0, 'giraffe': 0, 'backpack': 0, 'umbrella': 0, 'handbag': 0, 'tie': 0,
                 'suitcase': 1, 'frisbee': 0, 'skis': 0, 'snowboard': 0, 'sports ball': 0,
                 'kite': 0, 'baseball bat': 1, 'baseball glove': 1, 'skateboard': 0,
                 'surfboard': 0, 'tennis racket': 1, 'bottle': 1, 'wine glass': 1, 'cup': 1,
                 'fork': 1, 'knife': 1, 'spoon': 1, 'bowl': 1, 'banana': 1, 'apple': 1,
                 'sandwich': 1, 'orange': 1, 'broccoli': 0, 'carrot': 0, 'hot dog': 1, 'pizza': 1,
                 'donut': 1, 'cake': 1, 'chair': 1, 'couch': 1, 'potted plant': 0, 'bed': 1,
                 'dining table': 1, 'toilet': 1, 'tv': 1, 'laptop': 1, 'mouse': 1, 'remote': 1,
                 'keyboard': 1, 'cell phone': 1, 'microwave': 1, 'oven': 1, 'toaster': 1,
                 'sink': 1, 'refrigerator': 1, 'book': 1, 'clock': 1, 'vase': 1, 'scissors': 0,
                 'teddy bear': 1, 'hair drier': 1, 'toothbrush': 1}

def count_ojects(objLst):
  count = 0
  for objs in objLst:
    count+= myDict.get(objs)
  return count

import ast
img_info['bad_obj_count'] = img_info['object_names'].apply(lambda x: count_ojects(ast.literal_eval(x)))

np.percentile(img_info.bad_obj_count, range(0,105,5))

high_bad_obj_count = img_info.loc[img_info.bad_obj_count>=thresh_bad_obj_count, :].reset_index(drop=True)
high_bad_obj_count['high_bad_obj_count'] = 1

df = df.merge(right=high_bad_obj_count[['id', 'high_bad_obj_count']], how='left')
df['high_bad_obj_count'] =  df['high_bad_obj_count'].fillna(0)
df.loc[df.high_bad_obj_count==1, 'landmarks'] = np.nan # remove predictions when lots of non-landmarks objects


## remove chairs??
df[['id', 'landmarks']].to_csv('../subs/last-submission/submission-ResNet50-ensemble-HM{}-CNT{}.csv'.format(thresh, thresh_bad_obj_count), index=False)

df.sample(10)
