#!/usr/local/bin/python3
"""
Alex-Antoine Fortin
March 25th 2018
Description
This python script:
1. Import needed modules
2. Defines model (ResNet34 trained on ImageNet)
4. Freezes layers, Allow training on classifier layers only
5. Training
"""

import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)

import os, threading, numpy as np, imp
#from keras.preprocessing.image import ImageDataGenerator
from time import time
t = imp.load_source('tools', '../code/recognition/tools.py')
startTime = time()
TARGET_SIZE = (200, 200) # (197, 197) minimum for ResNet50
BATCH_SIZE = 102

#==============================
#Generating and cropping images
#==============================
genTrain = t.genTrain()
traingen = t.traingen(path='../masked_inputs/smaller_train', genTrain=genTrain,
                      BATCH_SIZE=BATCH_SIZE, RANDOM_CROP_SIZE=TARGET_SIZE)

genVal = t.genTrain()
valgen = t.traingen(path='../masked_inputs/smaller_val', genTrain=genVal,
                      BATCH_SIZE=BATCH_SIZE, RANDOM_CROP_SIZE=TARGET_SIZE)

print("The process took {}s to initiate.".format(int(time()-startTime)))

#=============================
#Model Specs and Model Fitting
#=============================
from keras import backend as K
from keras.optimizers import Adam, SGD
from keras.callbacks import LearningRateScheduler
from keras.utils import multi_gpu_model
from keras_contrib import *
from keras.models import load_model
MODEL_NAME = 'ResNet18'
LOAD_MODEL_FROM_FILE = 'Epoch10-Loss5.619795-Acc0.109619.h5'
START_TRAINING_ON_EPOCH = 11
NB_EPOCH = 25

OUTPUT_FC1_DIM = None #5000
NB_CLASS = traingen.num_classes
INIT_LR = 0.003
# Instanciating the model on CPU & CPU memory
t = imp.load_source('tools', '../code/recognition/tools.py')
with tf.device('/cpu:0'):
    if type(LOAD_MODEL_FROM_FILE)==type(None):
        model = t.define_model(TARGET_SIZE, OUTPUT_FC1_DIM, NB_CLASS, MODEL_NAME)
        assert START_TRAINING_ON_EPOCH == 0
        print('Initialized model and froze weights except for top')
    elif isinstance(LOAD_MODEL_FROM_FILE, str):
        from resnet.resnet101 import Scale
        model = load_model('../models/{}/{}'.format(MODEL_NAME, LOAD_MODEL_FROM_FILE), custom_objects=Scale)
        print('Loaded model from file: {}'.format(LOAD_MODEL_FROM_FILE))

print(model.summary())

#optimizer = SGD(lr=INIT_LR, decay=0, momentum=0.9, nesterov=True)
optimizer = Adam(lr=INIT_LR) #XXX: consider amsgrad=True

#=======================================================
# compile the model
# should be done *after* setting layers to non-trainable
#=======================================================
parallel_model = multi_gpu_model(model, gpus=3)

#==========
# Callbacks
#==========
def scheduler(epoch):
    # Code executed on_epoch_begin
    if epoch in [11,12,13,14,15,16]:
        new_lr = 0.0008
    elif epoch in [17,18,19,20,21,22,23,24]:
        new_lr = 0.0003
    elif epoch >= 25:
        new_lr = 0.0001
    else:
        new_lr = K.get_value(parallel_model.optimizer.lr)
    print("The learning rate is: {}".format(new_lr))
    return new_lr

lr_adjustment = LearningRateScheduler(scheduler) # every 10

parallel_model.compile(optimizer=optimizer, loss='categorical_crossentropy',
              metrics=['accuracy'])


print("NB_EPOCH:{}".format(NB_EPOCH))
print("NB_CLASS:{}".format(NB_CLASS))

for current_epoch in range(START_TRAINING_ON_EPOCH,NB_EPOCH,1):
    # Resume training
    hist = parallel_model.fit_generator(
            generator = traingen,
            steps_per_epoch = traingen.n//BATCH_SIZE,
            validation_data = valgen,
            validation_steps = valgen.n//BATCH_SIZE,
            epochs = current_epoch+1,
            workers = 20,
            callbacks = [lr_adjustment],
            initial_epoch = current_epoch,
            use_multiprocessing=True)

    # Save after each epoch
    os.makedirs('../models/{}'.format(MODEL_NAME), exist_ok=True)
    model.save('../models/{}/Epoch{:02d}-Loss{:08.6f}-Acc{:08.6f}.h5'.format(MODEL_NAME, hist.epoch[0]+1, hist.history['val_loss'][-1], hist.history['val_acc'][-1]))

#Save before leaving
model.save('../models/{}/Epoch{:02d}-Loss{:08.6f}-Acc{:08.6f}.h5'.format(MODEL_NAME, hist.epoch[0]+1, hist.history['val_loss'][-1], hist.history['val_acc'][-1]))
