"""
Alex-Antoine Fortin
Tuesday April 24th 2018
Description
Do the following steps
[ ] if train has less than 30 img, copy 30 img at random over
[ ] if train has between 31 and 199 images, copy the bulk of images over
[ ] if train has 200 images or more, choose 200 at random to copy over
"""

input_dir = '../masked_inputs/train'
target_dir = '../masked_inputs/smaller_val/'
first_t, second_t = 30, 200 # smaller_train
# first_t, second_t = 10, 50  # smaller_val

import os, random
from tqdm import tqdm
from shutil import copyfile

lst_directories = os.listdir(input_dir)

for my_dir in tqdm(lst_directories):
    # make the new directory if it does not exists
    os.makedirs(os.path.join(target_dir, my_dir), exist_ok=True)
    lst_files = os.listdir(os.path.join(input_dir, my_dir))
    count = len(lst_files)
    if count < first_t:
        lst_to_copy = (lst_files*first_t)[:first_t]
    elif ((first_t < count) & (count < second_t)): # copy the whole thing
        lst_to_copy = lst_files
    else: # x > second_t --> keep second_t items
        random.shuffle(lst_files)
        lst_to_copy = lst_files[:second_t]
    # Check on duplicate filenames
    lst_to_copy2 = []
    for idx, fn in enumerate(lst_to_copy):
        lst_to_copy2 += ['_'.join([str(idx), fn])]
    # Copy
    for fn1, fn2 in zip(lst_to_copy, lst_to_copy2):
        copyfile(os.path.join(input_dir, my_dir, fn1), os.path.join(target_dir, my_dir, fn2))
