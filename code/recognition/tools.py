#!/usr/local/bin/python3
"""
Alex-Antoine Fortin
Wednesday Dec 6, 2017
Description
This python script regroups functions used thorough our code
challenge
Originally developped for the universal style transfer algorithm
"""
import os, imp, collections, threading, numpy as np
from skimage.transform import resize as skimage_resize
from numba import jit
#from keras.preprocessing.image import ImageDataGenerator
image = imp.load_source('image', '../code/recognition/ImageGenerator.py')
from keras.callbacks import ModelCheckpoint
#===============
#Cropping images
#===============
@jit
def preprocess_input(x):
    x = x.astype('float32') / 255.
    x -= 0.5
    return x * 2

def postprocess_int(x):
    x /= 2
    x += 0.5
    return x * 255.

#resize_img is not used
def resize_img(img, resize=512):
    """
    Resize short side to resize
    @img: np.ndarray The image numpy.array to resize
    @resize: Int. The desired lenght of the short side of the image.
    """
    height, width = img.shape[0], img.shape[1]
    img = preprocess_input(img)
    if height < width:
        ratio = height / resize
        long_side = round(width / ratio)
        resize_shape = (resize, long_side, 3)
    else: #height > width
        ratio = width / resize
        long_side = round(height / ratio)
        resize_shape = (long_side, resize, 3)
    return skimage_resize(img, resize_shape, mode='reflect')

def center_crop(x, center_crop_size, **kwargs):
    """
    Returns a preprocessed centered crop of an array x
    """
    centerw, centerh = x.shape[1]//2, x.shape[2]//2
    halfw, halfh = center_crop_size[0]//2, center_crop_size[1]//2
    return preprocess_input(x[:, centerw-halfw:centerw+halfw,centerh-halfh:centerh+halfh])

def random_crop(x, random_crop_size, train_or_test='train', sync_seed=None):
    """
    Return a preprocessed random crop of a resized array x
    @random_crop_size: Tuple. (width, height, channels)
    @train_or_test: Str. 'train' to output a cropped array, 'test' to return a
    cropped array and count
    """
    assert train_or_test in ['train', 'test']
    np.random.seed(sync_seed)
    w, h = x.shape[0], x.shape[1]
    rangew = (w - random_crop_size[0])
    rangeh = (h - random_crop_size[1])
    #cropped = np.empty(shape=(random_crop_size[0], random_crop_size[1], random_crop_size[2]), dtype=float) #initialize empty array
    good_crop, count = False, 0
    while (good_crop==False and count<10):
        offsetw = 0 if rangew == 0 else np.random.randint(rangew)
        offseth = 0 if rangeh == 0 else np.random.randint(rangeh)
        cropped = x[offsetw:offsetw+random_crop_size[0], offseth:offseth+random_crop_size[1],:]
        if np.count_nonzero(cropped)/cropped.size > 0.55: # If (1 - 0.55) = 45% of the picture is black then get another crop from the same picture
            good_crop = True
        else:
            count+=1
            #print('Count:{}, percentage: {}'.format(count, np.count_nonzero(cropped)/cropped.size))
    if train_or_test == 'train':
        return preprocess_input(cropped)
    elif train_or_test =='test':
        return preprocess_input(cropped), count

#==================
# Defining data generator
#==================
def genTrain():
    return image.ImageDataGenerator(
    featurewise_center=False, # Set input mean to 0 over the dataset, feature-wise.
    samplewise_center=False, # Set each sample mean to 0.
    featurewise_std_normalization=False, # Divide inputs by std of the dataset, feature-wise
    samplewise_std_normalization=False, # Divide each input by its std
    zca_whitening=False, # Apply ZCA whitening.
    zca_epsilon=1e-6, # epsilon for ZCA whitening. Default is 1e-6
    rotation_range=5, # Int. Degree range for random rotations
    width_shift_range=0.05, # Float (fraction of total width). Range for random horizontal shifts.
    height_shift_range=0.05, # Float (fraction of total height). Range for random vertical shifts.
    shear_range=0., # Float. Shear Intensity (Shear angle in counter-clockwise direction as radian
    zoom_range=0., # Float or [lower, upper]. Range for random zoom. If a float, [lower, upper] = [1-zoom_range, 1+zoom_range].
    channel_shift_range=0., # Float. Range for random channel shifts.
    fill_mode='reflect', #  {"constant", "nearest", "reflect" or "wrap"}. Points outside the boundaries of the input are filled according to the given mode
    cval=0., # Value used for points outside the boundaries when fill_mode = "constant"
    horizontal_flip=True, # Randomly flip inputs horizontally.
    vertical_flip=False, # Randomly flip inputs vertically.
    rescale=None, # If None or 0, no rescaling is applied, otherwise we multiply the data by the value provided (before applying any other transformation)
    #preprocessing_function=preprocess_input, # The function should take one argument: one image (Numpy tensor with rank 3), and should output a Numpy tensor with the same shape.
    data_format='channels_last')

def traingen(path, genTrain, BATCH_SIZE, RANDOM_CROP_SIZE):
    return genTrain.flow_from_directory(
        directory=path,
        target_size= None, # default: (256, 256)
        color_mode="rgb", # one of "grayscale", "rbg"
        classes=None, # If not provided, does not return a label.
        batch_size=BATCH_SIZE, # default: 32.
        shuffle=True,
        #seed=1234,
        #save_to_dir='../generated_img', #Path to directory where to save generated pics
        class_mode='categorical', #one of "categorical", "binary", "sparse" or None
        random_crop=random_crop,
        random_crop_size=RANDOM_CROP_SIZE
        )

#=======================================
# Adding random cropping to a generators
#=======================================
class crop_gen:
    """
    Uses @generator to get 1 batch.
    crop_gen randomly select a crop of size @target_size from an image obtained
    via @generator. It then return the randomly cropped image and its label if
    @with_label.
    """
    def __init__(self, generator,target_size):
        self.lock = threading.Lock()
        self.generator = generator
        self.target_size = target_size
    #
    def __iter__(self):
        return self
    #
    def __next__(self):
        with self.lock:
            batch = next(self.generator)
        return (random_crop(batch[0], self.target_size),batch[1])

def cp(path_to_save, save_weights_only=False):
    """
    Defines model checkpoint
    """
    if not os.path.isdir(path_to_save):
        os.makedirs(path_to_save)
    return ModelCheckpoint(filepath=os.path.join(path_to_save,
                        'model-{epoch:03d}-{val_loss:.4f}.hdf5'),
                        monitor='mse',
                        mode='min', # min: for loss, max: for acc, auto: infer from monitor's value
                        save_best_only=False, # if True, the latest best model according to the quantity monitored will not be overwritten
                        save_weights_only=save_weights_only, # model.save_weights(filepath) if True else model.save(filepath)
                        period=2 # Perform ModelCheckpoint every period epoch
                        )

#================
#Model definition
#================
from keras.models import Model
from keras.layers import Dense, Dropout, Activation, Flatten, Input
from keras.layers import GlobalAveragePooling2D, BatchNormalization
from keras import regularizers
from keras import backend as K
from keras.callbacks import TensorBoard, ModelCheckpoint, LearningRateScheduler

def define_model(TARGET_SIZE, OUTPUT_FC1_DIM, NB_CLASS, MODEL_NAME='ResNet50'):
    """
    @TARGET_SIZE: Tuple. Size of each input images.
    """
    if MODEL_NAME.lower()=='resnet34':
        from keras_contrib.applications.resnet import ResNet34
        print('loaded ResNet34')
        return ResNet34(  input_shape=(TARGET_SIZE[0], TARGET_SIZE[1], 3),
                        classes=NB_CLASS)
    elif MODEL_NAME.lower()=='resnet18':
        from keras_contrib.applications.resnet import ResNet18
        print('loaded ResNet34')
        return ResNet18(  input_shape=(TARGET_SIZE[0], TARGET_SIZE[1], 3),
                        classes=NB_CLASS)
    elif MODEL_NAME.lower()=='resnet50':
        from keras.applications.resnet50 import ResNet50 as loaded_model
        print('loaded ResNet50')
    elif MODEL_NAME.lower()=='resnet101':
        from resnet import ResNet101 as loaded_model
        print('loaded ResNet101')
    elif MODEL_NAME.lower()=='resnet152':
        from resnet import ResNet152 as loaded_model
        print('loaded ResNet152')
    elif MODEL_NAME.lower()=='nasnet':
        from keras.applications.nasnet import NASNetLarge as loaded_model
        print('loaded NasNet')
    elif MODEL_NAME.lower()=='inceptionresnetv2':
        from keras.applications.inception_resnet_v2 import InceptionResNetV2 as loaded_model
        print('loaded inceptionresnetv2')
    elif MODEL_NAME.lower()=='vgg19':
        from keras.applications.vgg19 import VGG19 as loaded_model
        print('loaded VGG19')
    elif MODEL_NAME.lower()=='vgg16':
        from keras.applications.vgg16 import VGG16 as loaded_model
        print('loaded VGG16')
    input_layer = Input(shape=(TARGET_SIZE[0], TARGET_SIZE[1], 3),
                        name='InputLayer')
    base_model = loaded_model(
        include_top=False,
        weights='imagenet', # None (random init) or 'imagenet'
        input_tensor=input_layer,
        input_shape=(TARGET_SIZE[0], TARGET_SIZE[1], 3),
        pooling=None,
        classes=None # optional number of classes to classify images into, only to be specified if include_top is True, and if no weights argument is specified.
        )
    #==================
    #Model architecture
    #==================
    x = base_model.output
    #x = GlobalAveragePooling2D(name='AvgPooling2D_after_base_model')(x)
    x = Flatten(name='flatten_1')(x)
    x = BatchNormalization( axis=1, center=True, scale=True,
                            name='BN_after_AvgPooling')(x)
    if type(OUTPUT_FC1_DIM)!=type(None):
        x = Dense(OUTPUT_FC1_DIM, kernel_initializer='he_normal', activation='relu', name='Dense_out1')(x)
    predictions = Dense(NB_CLASS, activation='softmax', name='Dense_out2')(x)
    return Model(inputs=base_model.input, outputs=predictions)


def cp(path_to_save):
    """
    Defines model checkpoint
    """
    if not os.path.isdir(path_to_save):
        os.makedirs(path_to_save)
    return ModelCheckpoint(filepath=os.path.join(path_to_save,
                        'model-{epoch:03d}-{val_acc:.4f}-{val_loss:.4f}.hdf5'),
                        monitor='val_acc',
                        mode='max', # min: for loss, max: for acc, auto: infer from monitor's value
                        save_best_only=False, # if True, the latest best model according to the quantity monitored will not be overwritten
                        save_weights_only=False, # model.save_weights(filepath) if True else model.save(filepath)
                        period=2 # Perform ModelCheckpoint every period epoch
                        )
