import pandas as pd, numpy as np

SUBMISSION_FILE = '../submission-ResNet50-20180331-clocktime185316.csv'

df = pd.read_csv(SUBMISSION_FILE, header=0)

# Separating landmarks and score
df['landmarks'] = df.landmarks.str.rsplit()
df['score'] = df.landmarks.apply(lambda x: x[1] if type(x)==type([]) else None)
df['landmarks'] = df.landmarks.apply(lambda x: x[0] if type(x)==type([]) else None)

# Statistics
df['score'] = df.score.astype(float)
np.nanpercentile(df.score, range(15)) # about 7% of my predictions are below 10%

# Replacing values
df['score'] = df.score.apply(lambda x: x if x>0.10 else None)
df.loc[df.score.isnull(), 'landmarks'] = None

# Formatting for submission
df['landmarks'] = df[['landmarks', 'score']].apply(lambda x: ' '.join([str(x[0]),str(x[1])]) if type(x[0])!=type(None) else None, axis=1)

# Writing to disk
df[['id', 'landmarks']].to_csv('../submission-ResNet50-NoLowPred-20180331-clocktime185316.csv', index=False)
