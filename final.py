import pandas as pd
import numpy as np

df1 = pd.read_csv('../mask-rcnn-moto-car-bed_summary.csv', header=0)
df2 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-1.csv', header=0)
df3 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-3.csv', header=0)
df4 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-4.csv', header=0)
df5 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-5.csv', header=0)
df6 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-6.csv', header=0)
df7 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-7.csv', header=0)
df8 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-8.csv', header=0)
df9 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-9.csv', header=0)
df10 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-10.csv', header=0)
df11 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-11.csv', header=0)
df12 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-12.csv', header=0)
df13 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-13.csv', header=0)
df14 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-14.csv', header=0)
df15 = pd.read_csv('../mask-rcnn-moto-car-bed_summary-15.csv', header=0)
objects_detect = df1.append(df2, ignore_index=True).append(df3, ignore_index=True).append(df4, ignore_index=True)\
.append(df5, ignore_index=True).append(df6, ignore_index=True).append(df7, ignore_index=True).append(df8, ignore_index=True).append(df9, ignore_index=True)\
.append(df10, ignore_index=True).append(df11, ignore_index=True).append(df12, ignore_index=True)\
.append(df13, ignore_index=True).append(df14, ignore_index=True).append(df15, ignore_index=True)
objects_detect['id'] = objects_detect['img'].str[:-4]


# load current predictions
cur_preds = pd.read_csv('../subs/last-submission/ResNet50-ensemble-high-mask-0.65-handlab2.csv', header=0)

# merge
print(cur_preds.shape)
cur_preds = cur_preds.merge(right=objects_detect[['id', 'percent_area_masks_motorcycle', 'percent_area_masks_car', 'percent_area_masks_bed']], on='id', how='left')
print(cur_preds.shape)
cur_preds['percent_area_masks_motorcycle'] = cur_preds['percent_area_masks_motorcycle'].fillna(0)
cur_preds['percent_area_masks_car'] = cur_preds['percent_area_masks_car'].fillna(0)
cur_preds['percent_area_masks_bed'] = cur_preds['percent_area_masks_bed'].fillna(0)

# Exploration
#np.percentile(cur_preds['percent_area_masks_motorcycle'], range(80,101,1)) # thresh: 0.40 or 0.30
#np.percentile(cur_preds['percent_area_masks_car'], range(80,101,1)) # thresh: 0.40 or 0.35
#np.percentile(cur_preds['percent_area_masks_bed'], range(80,101,1)) # thresh: 0.20 or 0.10

#cur_preds.loc[((cur_preds['percent_area_masks_bed']>0.10) &(cur_preds['percent_area_masks_bed']<0.11)), ['id', 'percent_area_masks_bed']]
#cur_preds.loc[((cur_preds['percent_area_masks_bed']>0.20)), ['id', 'percent_area_masks_bed']].shape

print(cur_preds.loc[cur_preds.not_a_landmark==1, 'landmarks'].shape)
print(cur_preds.loc[cur_preds.percent_area_masks_motorcycle>=0.3, 'landmarks'].shape)
print(cur_preds.loc[cur_preds.percent_area_masks_car>=0.35, 'landmarks'].shape)
print(cur_preds.loc[cur_preds.percent_area_masks_bed>=0.11, 'landmarks'].shape)

# Thresholding
cur_preds.loc[cur_preds.not_a_landmark==1, 'landmarks'] = np.nan
cur_preds.loc[cur_preds.percent_area_masks_motorcycle>=0.4, 'landmarks'] = np.nan #0.4 or 0.3
cur_preds.loc[cur_preds.percent_area_masks_car>=0.4, 'landmarks'] = np.nan #0.4 or 0.35
cur_preds.loc[cur_preds.percent_area_masks_bed>=0.15, 'landmarks'] = np.nan # 0.15 or 0.1

cur_preds[['id', 'landmarks']].to_csv('../subs/last-submission/ResNet50-ensemble-high-mask-0.65-2ndRound-conservativeBedCarMoto.csv', index=False)
